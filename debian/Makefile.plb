lib.name = wiimote
class.sources = \
	wiimote.c \
	$(empty)

datafiles = \
	$(wildcard *.pd) \
	README \
	CHANGES \
	LICENSE \
	$(empty)

datadirs = \
	$(empty)

PKG_CONFIG ?= pkg-config
cwiidcflags = $(shell $(PKG_CONFIG) --cflags cwiid)
cwiidlibs = $(shell $(PKG_CONFIG) --libs cwiid)

cflags = $(cwiidcflags)
ldlibs = $(cwiidlibs)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
